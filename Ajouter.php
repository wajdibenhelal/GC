<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ajouter</title>
	<link rel="stylesheet" href="css/bootstraptheme.min.css">
	<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
    	<div class="container">
	        <div class="navbar-header">
	          <a href="../GC" class="navbar-brand">Gestion Société</a>
	        </div>
			<ul class="nav navbar-nav">
		    	<li><a href="#">Consulter</a></li>    
		    	<li><a href="#">Ajouter</a></li>    
		    </ul>
		    <ul class="nav navbar-nav navbar-right">
	            <li><a href="http://builtwithbootstrap.com/" target="_blank">Helios</a></li>
			</ul>
		</div>
	</nav>
</body>
</html>