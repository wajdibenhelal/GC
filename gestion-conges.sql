-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 03 Février 2016 à 13:51
-- Version du serveur :  5.6.27-0ubuntu1
-- Version de PHP :  5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestion-conges`
--

-- --------------------------------------------------------

--
-- Structure de la table `conge`
--

CREATE TABLE IF NOT EXISTS `conge` (
  `id` int(11) NOT NULL,
  `date_debut` varchar(255) NOT NULL,
  `nbr_jours_conge` float NOT NULL,
  `status` varchar(255) NOT NULL,
  `id_employe` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `conge`
--

INSERT INTO `conge` (`id`, `date_debut`, `nbr_jours_conge`, `status`, `id_employe`) VALUES
(3, '2016-02-12', 2, 'En Attente', 5),
(4, '2016-02-12', 2, 'En Attente', 3),
(5, '2016-02-27', 1, 'En Attente', 2),
(6, '2016-02-19', 0.5, 'Refuse', 3),
(7, '2016-02-11', 4, 'Accepte', 4);

-- --------------------------------------------------------

--
-- Structure de la table `division`
--

CREATE TABLE IF NOT EXISTS `division` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `id_chef` int(11) NOT NULL,
  `id_directeur` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `division`
--

INSERT INTO `division` (`id`, `nom`, `description`, `id_chef`, `id_directeur`) VALUES
(1, 'Dev', 'developpement', 2, 4),
(2, 'maint', 'maintenance', 2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE IF NOT EXISTS `employe` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `ncin` int(11) NOT NULL,
  `nbr_jours_conge` float NOT NULL,
  `role` varchar(255) NOT NULL,
  `id_division` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `employe`
--

INSERT INTO `employe` (`id`, `nom`, `ncin`, `nbr_jours_conge`, `role`, `id_division`) VALUES
(1, 'Directeur G', 4466515, 30, 'Directeur General', 0),
(2, 'Chef Div', 52654, 30, 'Chef Division', 0),
(3, 'D admini', 24524, 30, 'Directeur Administratif', 0),
(4, 'D Tech', 254514, 26, 'Directeur Technique', 0),
(5, 'Employe 1', 2454, 30, 'Employe', 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `conge`
--
ALTER TABLE `conge`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `conge`
--
ALTER TABLE `conge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `employe`
--
ALTER TABLE `employe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
